<%-- 
    Document   : adminAgregarProducto
    Created on : 04-06-2017, 20:36:47
    Author     : Nicolas Recabarren
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <link href="./css/font-awesome.min.css" rel="stylesheet">
        <link href="./css/style.css" rel="stylesheet">
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
        <script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="./js/bootstrap.min.js"></script>
        <script type="text/javascript" src="./js/funciones.js"></script>
    </head>
    <body>
        <header>
            <nav class="navbar navbar-static-top">
                <div class="container-fluid">
                    <div class="mobile-bars navbar-left hidden-md hidden-lg col-sm-4 col-xs-4">
                        <a href="javascript:;">
                            <i class="fa fa-bars fa-2x"></i>
                        </a>
                    </div>
                    <div class="navbar-header col-sm-4 col-xs-4">
                        <a class="navbar-brand" href="javascript:;">FUKUSUKE</a>
                    </div>
                    <div class="navbar-right col-sm-8 col-xs-8 text-right hidden-sm hidden-xs">
                        <ul class="nav navbar-nav" style="float: right;">
                            <li>
                                <a href="admin_mantenedor_clientes.html">Clientes</a>
                            </li>
                            <li>
                                <a href="AdminListadoProductos" class="active">Productos</a>
                            </li>
                            <li>
                                <a href="AdminListadoCategorias">Categorías</a>
                            </li>
                            <li>
                                <a href="admin_mantenedor_usuarios.html">Usuarios</a>
                            </li>
                            <li>
                                <a href="reportes.html">Reportes</a>
                            </li>
                            <li>
                                <a href="login.html">Salir</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="mobile-menu hide">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="admin_mantenedor_clientes.html">Clientes</a>
                        </li>
                        <li>
                            <a href="AdminListadoProductos" class="active">Productos</a>
                        </li>
                        <li>
                            <a href="AdminListadoCategorias">Categorías</a>
                        </li>
                        <li>
                            <a href="admin_mantenedor_usuarios.html">Usuarios</a>
                        </li>
                        <li>
                            <a href="reportes.html">Reportes</a>
                        </li>
                        <li>
                            <a href="login.html">Salir</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <section class="row fixed-padding content">
            <div class="col-md-10 col-md-offset-1">
                <div class="admin-section-title">
                    <h1>Agregar Producto</h1>
                </div>
            </div>
            <div class="col-md-10 col-md-offset-1">
                <form name="AgregarProducto" method="post" class="admin-form" action="AdminAgregarProducto">
                    <fieldset class="form-group">
                        <label>Nombre</label>
                        <input type="text" name="nombre">
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Descripción</label>
                        <input type="text" name="descripcion">
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Precio</label>
                        <input type="number" name="precio">
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Cantidad Porciones</label>
                        <input type="number" name="cantPorciones">
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Categoría</label>
                        <select name="categoriaId">
                            <option value="">Seleccione</option>
                            <c:forEach var="categoria" items="${listadoCategorias}" >
                                <option value="<c:out value="${categoria.id}"></c:out>"><c:out value="${categoria.descripcion}"></c:out></option>
                            </c:forEach>
                        </select>
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Stock</label>
                        <input type="number" name="stock">
                    </fieldset>
                    <fieldset class="form-group">
                        <label>Imagen</label>
                        <input type="file" name="imagen">
                    </fieldset>
                    <a href="AdminListadoProductos" class="btn btn-cancel">Cancelar</a>
                    <button type="submit" class="btn btn-add">Guardar</button>
                </form>
            </div>
        </section>
        <footer>
            <p><span class="bold">¿Contacto? Escríbenos a:</span> <br /><span class="underline">contacto@fukusuke.cl</span></p>
        </footer>
    </body>
</html>