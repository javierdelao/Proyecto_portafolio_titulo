<%-- 
    Document   : adminListadoCategorias
    Created on : 04-06-2017, 22:27:36
    Author     : Nicolas Recabarren
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <link href="./css/font-awesome.min.css" rel="stylesheet">
        <link href="./css/style.css" rel="stylesheet">
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
        <script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    </head>
    <body>
        <header>
            <nav class="navbar navbar-static-top">
                <div class="container-fluid">
                    <div class="mobile-bars navbar-left hidden-md hidden-lg col-sm-4 col-xs-4">
                        <a href="javascript:;">
                            <i class="fa fa-bars fa-2x"></i>
                        </a>
                    </div>
                    <div class="navbar-header col-sm-4 col-xs-4">
                        <a class="navbar-brand" href="javascript:;">FUKUSUKE</a>
                    </div>
                    <div class="navbar-right col-sm-8 col-xs-8 text-right hidden-sm hidden-xs">
                        <ul class="nav navbar-nav" style="float: right;">
                            <li>
                                <a href="admin_mantenedor_clientes.html">Clientes</a>
                            </li>
                            <li>
                                <a href="AdminListadoProductos">Productos</a>
                            </li>
                            <li>
                                <a href="AdminListadoCategorias" class="active">Categorías</a>
                            </li>
                            <li>
                                <a href="admin_mantenedor_usuarios.html">Usuarios</a>
                            </li>
                            <li>
                                <a href="reportes.html">Reportes</a>
                            </li>
                            <li>
                                <a href="login.html">Salir</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="mobile-menu hide">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="admin_mantenedor_clientes.html">Clientes</a>
                        </li>
                        <li>
                            <a href="AdminListadoProductos">Productos</a>
                        </li>
                        <li>
                            <a href="AdminListadoCategorias" class="active">Categorías</a>
                        </li>
                        <li>
                            <a href="admin_mantenedor_usuarios.html">Usuarios</a>
                        </li>
                        <li>
                            <a href="reportes.html">Reportes</a>
                        </li>
                        <li>
                            <a href="login.html">Salir</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <section class="row fixed-padding content">
            <div class="col-md-10 col-md-offset-1">
                <div class="admin-section-title">
                    <h1>Listado de Categorías</h1>
                </div>
            </div>
            <div class="col-md-10 col-md-offset-1">
                <table class="table table-hover" style="margin-top: 25px;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Descripción</th>
                            <th class="actions">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="categoria" items="${listadoCategorias}" >
                            <tr>
                                <td><c:out value="${categoria.id}"></c:out></td>
                                <td><c:out value="${categoria.descripcion}"></c:out></td>
                                <td class="actions">
                                    <a href="AdminEditarCategoria?id=<c:out value="${categoria.id}"></c:out>"><i class="fa fa-edit" title="Editar"></i></a>&nbsp;
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <a href="AdminAgregarCategoria" class="btn btn-red" style="margin-top: 20px; display: inline-block;">Agregar Categoría</a>
            </div>
        </section>
        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="modalInfoTitle" id="advertencia">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="modalInfoTitle">Advertencia</h4>
                    </div> 
                    <div class="modal-body">
                        ¿Está seguro que desea ejecutar esta acción?
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <p><span class="bold">¿Contacto? Escríbenos a:</span> <br /><span class="underline">contacto@fukusuke.cl</span></p>
        </footer>
    </body>
</html>
