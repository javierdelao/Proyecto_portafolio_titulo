/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author javier
 */
@Entity
@Table(name = "VENTA_DIARIA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VentaDiaria.findAll", query = "SELECT v FROM VentaDiaria v")
    , @NamedQuery(name = "VentaDiaria.findById", query = "SELECT v FROM VentaDiaria v WHERE v.id = :id")
    , @NamedQuery(name = "VentaDiaria.findByProductoId", query = "SELECT v FROM VentaDiaria v WHERE v.productoId = :productoId")
    , @NamedQuery(name = "VentaDiaria.findByCantidad", query = "SELECT v FROM VentaDiaria v WHERE v.cantidad = :cantidad")
    , @NamedQuery(name = "VentaDiaria.findByFecha", query = "SELECT v FROM VentaDiaria v WHERE v.fecha = :fecha")
    , @NamedQuery(name = "VentaDiaria.findByMonto", query = "SELECT v FROM VentaDiaria v WHERE v.monto = :monto")})
public class VentaDiaria implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private BigDecimal id;
    @Column(name = "PRODUCTO_ID")
    private BigInteger productoId;
    @Column(name = "CANTIDAD")
    private BigInteger cantidad;
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Column(name = "MONTO")
    private BigInteger monto;

    public VentaDiaria() {
    }

    public VentaDiaria(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigInteger getProductoId() {
        return productoId;
    }

    public void setProductoId(BigInteger productoId) {
        this.productoId = productoId;
    }

    public BigInteger getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigInteger cantidad) {
        this.cantidad = cantidad;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigInteger getMonto() {
        return monto;
    }

    public void setMonto(BigInteger monto) {
        this.monto = monto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VentaDiaria)) {
            return false;
        }
        VentaDiaria other = (VentaDiaria) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.VentaDiaria[ id=" + id + " ]";
    }
    
}
