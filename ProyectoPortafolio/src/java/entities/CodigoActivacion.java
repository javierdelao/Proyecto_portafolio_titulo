/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author javier
 */
@Entity
@Table(name = "CODIGO_ACTIVACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CodigoActivacion.findAll", query = "SELECT c FROM CodigoActivacion c")
    , @NamedQuery(name = "CodigoActivacion.findById", query = "SELECT c FROM CodigoActivacion c WHERE c.id = :id")
    , @NamedQuery(name = "CodigoActivacion.findByUsuarioId", query = "SELECT c FROM CodigoActivacion c WHERE c.usuarioId = :usuarioId")
    , @NamedQuery(name = "CodigoActivacion.findByFechaCreacion", query = "SELECT c FROM CodigoActivacion c WHERE c.fechaCreacion = :fechaCreacion")
    ,@NamedQuery(name = "CodigoActivacion.findLast", query = "SELECT c FROM CodigoActivacion c order by c.id desc")
    , @NamedQuery(name = "CodigoActivacion.findByOcupado", query = "SELECT c FROM CodigoActivacion c WHERE c.ocupado = :ocupado")
    , @NamedQuery(name = "CodigoActivacion.findByDisponible", query = "SELECT c FROM CodigoActivacion c WHERE c.disponible = :disponible")
    , @NamedQuery(name = "CodigoActivacion.findByCodigo", query = "SELECT c FROM CodigoActivacion c WHERE c.codigo = :codigo")})
public class CodigoActivacion implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USUARIO_ID")
    private BigInteger usuarioId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OCUPADO")
    private short ocupado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DISPONIBLE")
    private short disponible;
    @Size(max = 100)
    @Column(name = "CODIGO")
    private String codigo;

    public CodigoActivacion() {
    }

    public CodigoActivacion(BigDecimal id) {
        this.id = id;
    }

    public CodigoActivacion(BigDecimal id, BigInteger usuarioId, Date fechaCreacion, short ocupado, short disponible) {
        this.id = id;
        this.usuarioId = usuarioId;
        this.fechaCreacion = fechaCreacion;
        this.ocupado = ocupado;
        this.disponible = disponible;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigInteger getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(BigInteger usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public short getOcupado() {
        return ocupado;
    }

    public void setOcupado(short ocupado) {
        this.ocupado = ocupado;
    }

    public short getDisponible() {
        return disponible;
    }

    public void setDisponible(short disponible) {
        this.disponible = disponible;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CodigoActivacion)) {
            return false;
        }
        CodigoActivacion other = (CodigoActivacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.CodigoActivacion[ id=" + id + " ]";
    }
    
}
