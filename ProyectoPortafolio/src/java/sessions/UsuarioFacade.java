/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Usuario;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author javier
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {

    @PersistenceContext(unitName = "ProyectoPortafolioPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }
    
              public Usuario findByCorreo(String correo) {
                 Usuario usuario=null;
                  try {
                       usuario= em.createNamedQuery("Usuario.findByCorreo", Usuario.class)
                    .setParameter("correo", correo)
                     .setMaxResults(1)
                    .getSingleResult();
                  } catch (Exception e) {
                  }
         
         return usuario;
        
        }
              
    public Usuario findLast() {
         Usuario usuario= em.createNamedQuery("Usuario.findByIdDes", Usuario.class)
                    .setMaxResults(1)
                    .getSingleResult();
         return usuario;
        
    }
              
              
    
}
