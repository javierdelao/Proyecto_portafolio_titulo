/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.VentaDiaria;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author javier
 */
@Stateless
public class VentaDiariaFacade extends AbstractFacade<VentaDiaria> {

    @PersistenceContext(unitName = "ProyectoPortafolioPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VentaDiariaFacade() {
        super(VentaDiaria.class);
    }
    
}
