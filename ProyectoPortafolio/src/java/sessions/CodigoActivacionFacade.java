/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.CodigoActivacion;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author javier
 */
@Stateless
public class CodigoActivacionFacade extends AbstractFacade<CodigoActivacion> {

    @PersistenceContext(unitName = "ProyectoPortafolioPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CodigoActivacionFacade() {
        super(CodigoActivacion.class);
    }
    
        public CodigoActivacion findLast() {
         CodigoActivacion codigo= em.createNamedQuery("CodigoActivacion.findLast", CodigoActivacion.class)
                    .setMaxResults(1)
                    .getSingleResult();
         return codigo;
        
    }
             public CodigoActivacion findByCodigo(String codigoPar) {
                 CodigoActivacion codigo=null;
                 try {
                     codigo= em.createNamedQuery("CodigoActivacion.findByCodigo", CodigoActivacion.class)
                      .setParameter("codigo", codigoPar)
                      .setMaxResults(1)
                    .getSingleResult();
                 } catch (Exception e) {
                 }
          
         return codigo;
        
    }
    
}
