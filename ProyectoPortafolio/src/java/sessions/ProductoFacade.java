/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessions;

import entities.Categoria;
import entities.Producto;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author javier
 */
@Stateless
public class ProductoFacade extends AbstractFacade<Producto> {

    @PersistenceContext(unitName = "ProyectoPortafolioPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductoFacade() {
        super(Producto.class);
    }
    
       public List<Producto> findByCategoria(Categoria categoria) throws Exception{
        try{
            return em.createNamedQuery("Producto.findByCategoria", Producto.class)
                   .setParameter("categoriaId", categoria)
                   .getResultList();
        } catch (Exception e) {
            throw e;
        }
    }
    
}
