/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import entities.Categoria;
import entities.CodigoActivacion;
import entities.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sessions.CategoriaFacade;
import sessions.CodigoActivacionFacade;
import sessions.UsuarioFacade;

/**
 *
 * @author javier
 */
@WebServlet(name = "Activate", urlPatterns = {"/Activate"})
public class Activate extends HttpServlet {
    @EJB CodigoActivacionFacade sessionBeanCodigoActivacion;
    @EJB UsuarioFacade sessionBeanUsuario;
     @EJB CategoriaFacade sessionBeanCategoria;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
           List<Categoria> categorias=null;
        try{
            categorias = sessionBeanCategoria.findAll();
        } catch(Exception ex) { }

        request.setAttribute("categorias", categorias);

         String key=request.getParameter("key");

    
         CodigoActivacion codigo=sessionBeanCodigoActivacion.findByCodigo(key);
         if(codigo==null){
           request.setAttribute("typeMessage", "danger");
          request.setAttribute("errorMessage", "Codigo no Existe");
          RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
          rd.forward(request, response);
         return;
         }
   
         response.getWriter().write(key);
         int ocupado=codigo.getOcupado();
         int disponible=codigo.getDisponible();
         
         if(disponible==0){
         response.getWriter().write("Codigo no disponible");
          request.setAttribute("typeMessage", "danger");
          request.setAttribute("errorMessage", "Codigo no disponible");
          RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
          rd.forward(request, response);
         return;
        }
         if(ocupado==1){
         response.getWriter().write("Codigo Ocupado");
          request.setAttribute("typeMessage", "danger");
          request.setAttribute("errorMessage", "Codigo no disponible");
          RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
          rd.forward(request, response);
         return;
        }
         Usuario us=sessionBeanUsuario.find(Long.parseLong(codigo.getUsuarioId()+""));
 
         us.setActivo(Short.parseShort("1"));
         sessionBeanUsuario.edit(us);
         codigo.setDisponible(Short.parseShort(0+""));
         codigo.setOcupado(Short.parseShort(1+""));
         sessionBeanCodigoActivacion.edit(codigo);
          response.getWriter().write("Estimado "+us.getNombres()+" "+us.getApellidos());
         response.getWriter().write("  Su cuenta fue activada exitosamente");
            request.setAttribute("typeMessage", "success");
                 request.setAttribute("errorMessage", "Cuenta ACTIVADA Exitosamente, ahora puedes iniciar sesion");
                 RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
                 rd.forward(request, response);
         
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
