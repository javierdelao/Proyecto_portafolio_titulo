/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import Mail.Correo;
import Mail.MailController;
import entities.Categoria;
import entities.Ciudad;
import entities.Cliente;
import entities.CodigoActivacion;
import entities.Comuna;
import entities.Rol;
import entities.TipoCliente;
import entities.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.digest.DigestUtils;
import sessions.CategoriaFacade;
import sessions.CiudadFacade;
import sessions.ClienteFacade;
import sessions.CodigoActivacionFacade;
import sessions.ComunaFacade;
import sessions.RolFacade;
import sessions.TipoClienteFacade;
import sessions.UsuarioFacade;
import sun.nio.cs.ext.Big5;
/**
 *
 * @author Nicolas Recabarren
 */
@WebServlet(name = "Registro", urlPatterns = {"/Registro"})
public class Registro extends HttpServlet {
    
    @EJB CategoriaFacade sessionBeanCategoria;
    @EJB CiudadFacade sessionBeanCiudad;
    @EJB ComunaFacade sessionBeanComuna;
    @EJB UsuarioFacade sessionBeanUsuario;
    @EJB RolFacade sessionBeanRol;
    @EJB ClienteFacade sessionBeanCliente;
    @EJB TipoClienteFacade sessionBeanTipoCliente;
    @EJB CodigoActivacionFacade sessionBeanCodigoActivacion;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String type=request.getParameter("type");
        // Enviamos las categorías de productos para la barra lateral de la página.
        if(type==null){
        List<Categoria> categorias=null;
        try{
            categorias = sessionBeanCategoria.findAll();
        } catch(Exception ex) { }
        request.setAttribute("categorias", categorias);
        
        
        List<Ciudad> ciudades = null;
        try{
            ciudades = sessionBeanCiudad.findAll();
        } catch(Exception ex) { }
        request.setAttribute("ciudades", ciudades);
        
        
        List<Comuna> comunas = null;
        try{
            comunas = sessionBeanComuna.findAll();
        } catch(Exception ex) { }
        request.setAttribute("comunas", comunas);
        
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/registro.jsp");
        rd.forward(request, response);
        return;
        }
        String nombres=request.getParameter("nombres");
        String apellidos=request.getParameter("apellidos");
        String email=request.getParameter("email");
        String telefono=request.getParameter("telefono");
        Long provincia_id=Long.parseLong(request.getParameter("provincia_id"));
        Long comuna_id=Long.parseLong(request.getParameter("comuna_id"));
        String direccion=request.getParameter("Direccion");
        String password=request.getParameter("password");
        response.getWriter().write(nombres+"<br>");
        response.getWriter().write(apellidos+"<br>");
        response.getWriter().write(email+"<br>");
        response.getWriter().write(telefono+"<br>");
        response.getWriter().write(provincia_id+"<br>");
        response.getWriter().write(comuna_id+"<br>");
        response.getWriter().write(direccion+"<br>");
        response.getWriter().write(password+"<br>");
        
        Usuario us=null;
        us=sessionBeanUsuario.findByCorreo(email);
        if(us!=null){
            request.setAttribute("errorMessage", "Este email ya posee una cuenta asociada");
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/registro.jsp");
            rd.forward(request, response);
            return;
        }
        us=new Usuario(Long.parseLong("1"), email, password, nombres, apellidos);
        us.setActivo(Short.parseShort("0"));
        Rol rol=sessionBeanRol.find(Long.parseLong("2"));
        us.setRolId(rol);
        response.getWriter().write(rol.getDescripcion()+"<br>");
        sessionBeanUsuario.create(us);
        us=sessionBeanUsuario.findByCorreo(email);
        response.getWriter().write("id : "+us.getId()+"<br>");
        //id direccion telefono
        Cliente cli = new Cliente(Long.parseLong("1"),direccion,telefono);
        Comuna com=sessionBeanComuna.find(comuna_id);
        cli.setComunaId(com);
        TipoCliente tc=sessionBeanTipoCliente.find(Long.parseLong("1"));
        cli.setTipoClienteId(tc);
        cli.setUsuarioId(us);
        sessionBeanCliente.create(cli);
       
       CodigoActivacion codigoAnterior=sessionBeanCodigoActivacion.findLast();
       //BigDecimal id, BigInteger usuarioId, Date fechaCreacion, short ocupado, short disponible
        int number=Integer.parseInt(codigoAnterior.getId()+"")+1;
        BigDecimal bigId = new BigDecimal(number+"");
        BigInteger bi = BigInteger.valueOf(us.getId().intValue());
        Date fechaCreacion =new Date();
       CodigoActivacion codigo=new CodigoActivacion(bigId, bi, fechaCreacion, Short.parseShort("0"), Short.parseShort("1"));
        String textoSinEncriptar=bigId+""; 
        String textoEncriptadoConMD5=DigestUtils.md5Hex(textoSinEncriptar);
        codigo.setCodigo(textoEncriptadoConMD5);
        sessionBeanCodigoActivacion.create(codigo);
        
         Correo c=new Correo();
        c.setUsuarioCorreo("jdelaonova@gmail.com");
        c.setPass("zguhwbnrmlgrombc");
        c.setDestino(email);
        c.setRutaArchivo("");
        System.out.println(c.getRutaArchivo());
        c.setNombreArchivo("");
        c.setAsunto("Activacion de cuenta FUKUSUKE SUSHI");
        c.setMensaje("<h1>Estimado haga Click en el enlace para activar su cuenta</h1><br><a href='http://localhost:12764/ProyectoPortafolio/Activate?key="+textoEncriptadoConMD5+"'>http://localhost:12764/ProyectoPortafolio/Activate?key="+textoEncriptadoConMD5+"</a><br><label>Este link solo estará disponible para el dia de envío de este correo<label>");
        c.setRutaArchivo("");
        MailController ctrl=new MailController();
        String envio=ctrl.enviarCorreo(c);
        if(envio.equals("enviado")){
             response.getWriter().write("Correo Enviado<br>");
             request.setAttribute("typeMessage", "success");
             request.setAttribute("errorMessage", "Cuenta Creada, para activar su cuenta haga click en el enlace que le hemos enviado a "+email);
             RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
             rd.forward(request, response);
             return;
        }else{
              request.setAttribute("typeMessage", "danger");
             request.setAttribute("errorMessage", "No hemos podido enviar su correo , estamos trabajando para solucionarlo");
             RequestDispatcher rd = getServletContext().getRequestDispatcher("/registro.jsp");
             rd.forward(request, response);
             return;
        }
        
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
