/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;
import entities.Producto;
import entities.Categoria;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sessions.ProductoFacade;
import sessions.CategoriaFacade;

/**
 *
 * @author Nicolas Recabarren
 */
@WebServlet(name = "AdminEditarProducto", urlPatterns = {"/AdminEditarProducto"})
public class AdminEditarProducto extends HttpServlet {

    @EJB ProductoFacade sessionBeanProducto;
    @EJB CategoriaFacade sessionBeanCategoria;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        Long idProducto = Long.parseLong(request.getParameter("id"));
        // Buscamos el producto
        Producto producto = null;
        try{
            producto = sessionBeanProducto.find(idProducto);
        } catch(Exception exc){ }
        
        if( request.getParameter("nombre") == null){
            List<Categoria> categorias = null;
            try{
                categorias = sessionBeanCategoria.findAll();
            } catch(Exception exc) { }

            request.setAttribute("producto", producto);
            request.setAttribute("listadoCategorias", categorias);

            RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminEditarProducto.jsp");
            rd.forward(request, response);
            
        } else {
            String nombre = request.getParameter("nombre");
            String descripcion = request.getParameter("descripcion");
            String precio = request.getParameter("precio");
            String cantPorciones = request.getParameter("cantPorciones");
            String categoriaId = request.getParameter("categoriaId");
            String stock = request.getParameter("stock");
            
            Categoria nuevaCategoria = sessionBeanCategoria.find(Long.parseLong(categoriaId));
            
            producto.setNombre(nombre);
            producto.setDescripcion(descripcion);
            producto.setPrecio( Long.parseLong(precio) );
            producto.setStock( Long.parseLong(stock) );
            producto.setCategoriaId(nuevaCategoria);
            producto.setCantProciones( Long.parseLong(cantPorciones) );
            
            sessionBeanProducto.edit(producto);
            
            ServletContext contexto = request.getServletContext();
            response.sendRedirect(contexto.getContextPath() + "/AdminListadoProductos");
        }
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
