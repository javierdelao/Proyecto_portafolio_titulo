/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import entities.Categoria;
import entities.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sessions.CategoriaFacade;
import sessions.UsuarioFacade;
/**
 *
 * @author Nicolas Recabarren
 */
@WebServlet(name = "Login", urlPatterns = {"/Login"})
public class Login extends HttpServlet {
    
    @EJB CategoriaFacade sessionBeanCategoria;
    @EJB UsuarioFacade sessionBeanUsuario;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        // Enviamos las categorías de productos para la barra lateral de la página.
        List<Categoria> categorias=null;
        try{
            categorias = sessionBeanCategoria.findAll();
        } catch(Exception ex) { }

        request.setAttribute("categorias", categorias);
        
         String tipo=request.getParameter("type");
         
         if(tipo==null){
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
        rd.forward(request, response);
         }else if (tipo.equals("1")){
         String pass=request.getParameter("password");
         String correo=request.getParameter("email");
      
         Usuario us=null;
         
             us=sessionBeanUsuario.findByCorreo(correo);
             if(us==null){
                 
                 request.setAttribute("typeMessage", "danger");
                 request.setAttribute("errorMessage", " Usuario no existe");
                 RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
                 rd.forward(request, response);
                 return;
             }else{
                 if(!us.getClave().equals(pass)){
                  request.setAttribute("typeMessage", "danger");
                 request.setAttribute("errorMessage", "Contraseña o usuario no coinciden");
                 RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
                 rd.forward(request, response);
                 return;
                 }
                 
                 if(us.getActivo()==Short.parseShort("0")){
                 request.setAttribute("typeMessage", "danger");
                 request.setAttribute("errorMessage", "Usuario No habilitado");
                 RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
                 rd.forward(request, response);
                 return;
                 }
                 
                  if(us.getClave().equals(pass)){
                  request.setAttribute("typeMessage", "success");
                 request.setAttribute("errorMessage", "Bienvenido "+us.getNombres()+" "+us.getApellidos());
                 RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
                 rd.forward(request, response);
                 return;
                 }
                
             }
                   
                     
       
         }
        
        
        
        
        
        
  
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
