/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import entities.Categoria;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sessions.CategoriaFacade;

/**
 *
 * @author Nicolas Recabarren
 */
@WebServlet(name = "AdminEditarCategoria", urlPatterns = {"/AdminEditarCategoria"})
public class AdminEditarCategoria extends HttpServlet {

    @EJB CategoriaFacade sessionBeanCategoria;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        Long idCategoria = Long.parseLong(request.getParameter("id"));
        // Buscamos la categoria
        Categoria categoria = null;
        try{
            categoria = sessionBeanCategoria.find(idCategoria);
        } catch(Exception exc){ }
        
        if( request.getParameter("descripcion") == null){
            
            request.setAttribute("categoria", categoria);

            RequestDispatcher rd = getServletContext().getRequestDispatcher("/adminEditarCategoria.jsp");
            rd.forward(request, response);
            
        } else {
            String descripcion = request.getParameter("descripcion");
            
            categoria.setDescripcion(descripcion);
            sessionBeanCategoria.edit(categoria);
            
            ServletContext contexto = request.getServletContext();
            response.sendRedirect(contexto.getContextPath() + "/AdminListadoCategorias");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
