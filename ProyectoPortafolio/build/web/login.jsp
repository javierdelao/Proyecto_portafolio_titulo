<%-- 
    Document   : login
    Created on : 16/05/2017, 11:49:09 AM
    Author     : javier
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <link href="./css/font-awesome.min.css" rel="stylesheet">
        <link href="./css/style.css" rel="stylesheet">
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
        <script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    </head>
    <body>
        <header>
            <nav class="navbar navbar-static-top">
                <div class="container-fluid">
                    <div class="mobile-bars navbar-left hidden-md hidden-lg col-sm-4 col-xs-4">
                        <a href="javascript:;">
                            <i class="fa fa-bars fa-2x"></i>
                        </a>
                    </div>
                    <div class="navbar-header col-sm-4 col-xs-4">
                        <a class="navbar-brand" href="javascript:;">FUKUSUKE</a>
                    </div>
                    <div class="navbar-right col-sm-4 col-xs-4 text-right">
                        <ul class="nav navbar-nav hidden-sm hidden-xs">
                           <li>
                                <a href="index" class="active">Home</a>
                            </li>
                            <li>
                                <a href="ListadoProductos">Carta</a>
                            </li>
                            <li>
                                <a href="Login">Ingresar</a>
                            </li>
                        </ul>
                        <a href="mis_pedidos.html" class="shopping-cart">
                            <i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
                <div class="mobile-menu hide">
                    <ul class="nav navbar-nav">
                       <li>
                                <a href="index" class="active">Home</a>
                            </li>
                            <li>
                                <a href="ListadoProductos">Carta</a>
                            </li>
                            <li>
                                <a href="Login">Ingresar</a>
                            </li>
                    </ul>
                </div>
            </nav>
        </header>
        <section class="row fixed-padding content">
            <div class="sidebar-menu col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-3 hidden-xs hidden-sm">
                <h2>FUKUSUKE</h2>
                <h3>Categorías</h3>
                <ul class="list-group">
                    <c:forEach var="categoria" items="${categorias}" >
                        <li>
                            <a href="ListadoProductos?id=<c:out value="${categoria.id}" ></c:out>">
                                <c:out value="${categoria.descripcion}" ></c:out>
                            </a>
                        </li>
                    </c:forEach>
                </ul>
            </div>
            <div class="main-content col-md-7">
                <div class="login-box">
                    <div class="login-box-header">
                        <span>LOGIN</span>
                    </div>
                    <div class="login-box-content">
                        <form method="post" action="Login">
                            <input type="hidden" name="type" value="1">
                           
                             
                             <c:set var = "message" scope = "request" value = "${errorMessage}"/>
                            <c:if test="${message!=null}">
                            <div class="alert alert-<c:out value="${typeMessage}" ></c:out>">
                               <c:out value="${errorMessage}" ></c:out>
                            </div>
                            </c:if>
                            
                        
                            <div class="form-group">
                                <label>Correo electrónico:</label>
                                <input type="email" name="email">
                            </div>
                            <div class="form-group">
                                <label>Contraseña:</label>
                                <input type="password" name="password" class="has-error">
                            </div>
                            
                            <p class="registro-text">¿No tienes una cuenta? <a href="Registro" title="Registrarse">Regístrate aquí</a>.</p>
                            <div class="submit">
                                <button type="submit" class="btn btn-success">Ingresar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <footer>
            <p><span class="bold">¿Contacto? Escríbenos a:</span> <br /><span class="underline">contacto@fukusuke.cl</span></p>
        </footer>
    </body>
</html>