<%-- 
    Document   : home
    Created on : 29-05-2017, 14:33:48
    Author     : Nicolas Recabarren
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="./css/bootstrap.min.css" rel="stylesheet">
        <link href="./css/font-awesome.min.css" rel="stylesheet">
        <link href="./css/style.css" rel="stylesheet">
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
        <script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="./js/bootstrap.min.js"></script>
        <script type="text/javascript" src="./js/funciones.js"></script>
    </head>
    <body>
        <header>
            <nav class="navbar navbar-static-top">
                <div class="container-fluid">
                    <div class="mobile-bars navbar-left hidden-md hidden-lg col-sm-4 col-xs-4">
                        <a href="javascript:;">
                            <i class="fa fa-bars fa-2x"></i>
                        </a>
                    </div>
                    <div class="navbar-header col-sm-4 col-xs-4">
                        <a class="navbar-brand" href="javascript:;">FUKUSUKE</a>
                    </div>
                    <div class="navbar-right col-sm-4 col-xs-4 text-right">
                        <ul class="nav navbar-nav hidden-sm hidden-xs">
                            <li>
                                <a href="index" class="active">Home</a>
                            </li>
                            <li>
                                <a href="ListadoProductos">Carta</a>
                            </li>
                            <li>
                                <a href="Login">Ingresar</a>
                            </li>
                        </ul>
                        <a href="mis_pedidos.html" class="shopping-cart">
                            <i class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
                <div class="mobile-menu hide">
                    <ul class="nav navbar-nav">
                      <li>
                                <a href="index" class="active">Home</a>
                            </li>
                            <li>
                                <a href="ListadoProductos">Carta</a>
                            </li>
                            <li>
                                <a href="Login">Ingresar</a>
                            </li>
                    </ul>
                </div>
            </nav>
        </header>
        <section class="row fixed-padding content">
            <div class="sidebar-menu col-lg-offset-1 col-md-offset-1 col-lg-2 col-md-3 hidden-xs hidden-sm">
                <h2>FUKUSUKE</h2>
                <h3>Categorías</h3>
                <ul class="list-group">
                    <c:forEach var="categoria" items="${categorias}" >
                        <li>
                            <a href="ListadoProductos?id=<c:out value="${categoria.id}" ></c:out>">
                                <c:out value="${categoria.descripcion}" ></c:out>
                            </a>
                        </li>
                    </c:forEach>
                </ul>
            </div>
            <div class="main-content col-md-7">
                <div class="main-image">
                    <div class="banner-title">
                        <span>Te preparamos el mejor sushi!</span>
                    </div>
                    <img src="./img/banner.jpg">
                </div>
                <div class="row">
                    <c:forEach var="producto" items="${productos}" >
                        <article class="ficha-sushi col-md-4">
                            <div class="ficha-imagen">
                                <p class="price">
                                    $ <c:out value="${producto.precio}" ></c:out>
                                </p>
                                <img src="./img/ficha.jpg">
                            </div>
                            <div class="ficha-descripcion">
                                <p class="nombre-sushi text-center">
                                    <c:out value="${producto.nombre}" ></c:out>
                                </p>
                                <p class="sushi-desc">
                                    <c:out value="${producto.descripcion}" ></c:out>
                                </p>
                                <div><a href="javascript:;" data-toggle="modal" data-target="#addCart">Agregar al Carrito</a></div>
                            </div>
                        </article>
                    </c:forEach>
                </div>
            </div>
        </section>
        
        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="modalInfoTitle" id="informationModal">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="modalInfoTitle">Información</h4>
                    </div> 
                    <div class="modal-body">
                        Para agregar productos, primero debe iniciar sesión.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <a href="login.html" class="btn btn-red">Iniciar Sesión</a>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="modalInfoTitle2" id="addCart">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="modalInfoTitle2">Información</h4>
                    </div> 
                    <div class="modal-body">
                        ¡Producto agregado al carrito correctamente!
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <p><span class="bold">¿Contacto? Escríbenos a:</span> <br /><span class="underline">contacto@fukusuke.cl</span></p>
        </footer>
    </body>
</html>
